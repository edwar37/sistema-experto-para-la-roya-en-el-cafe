# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 23:24:52 2017

@author: edwarjavier
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from sistemaExpertoUI import *

"""Este ejecutable lanza la aplicación de escritorio. Para ver su funcionamiento ejecutar en consola
lo siguiente: python UI_init.py"""

app = QApplication(sys.argv)
window = QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(window)

window.show()
sys.exit(app.exec_())