# -*- coding: utf-8 -*-
"""
Created on Fri Dec  8 22:57:24 2017

@author: edwarjavier
"""

import sys
from PyQt5.QtWidgets import QApplication, QDialog, QPushButton,QLabel
from PyQt5 import uic
class Dialogo(QDialog):
    
    def __init__(self):
        QDialog.__init__(self)
        self.resize(300,300)
        self.setWindowTitle('Ventana emergente')
        self.etiqueta = QLabel(self)
        uic.loadUi('ventana2.ui',self)