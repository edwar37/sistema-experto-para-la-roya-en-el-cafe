# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 23:46:34 2017

@author: edwarjavier
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from radiobutton import *

app = QApplication(sys.argv)
window = QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(window)

window.show()
sys.exit(app.exec_())