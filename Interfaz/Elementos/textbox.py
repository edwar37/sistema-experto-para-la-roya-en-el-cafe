# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Caja_Texto.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(742, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label_Titulo = QtWidgets.QLabel(self.centralwidget)
        self.label_Titulo.setGeometry(QtCore.QRect(280, 30, 191, 51))
        self.label_Titulo.setSizeIncrement(QtCore.QSize(9, 9))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_Titulo.setFont(font)
        self.label_Titulo.setObjectName("label_Titulo")
        self.textEditar = QtWidgets.QTextEdit(self.centralwidget)
        self.textEditar.setGeometry(QtCore.QRect(150, 220, 481, 71))
        self.textEditar.setObjectName("textEditar")
        self.btnMostrar = QtWidgets.QPushButton(self.centralwidget)
        self.btnMostrar.setGeometry(QtCore.QRect(280, 390, 221, 81))
        self.btnMostrar.setObjectName("btnMostrar")
        self.btnMostrar.clicked.connect(self.on_click_mostrar)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 742, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_Titulo.setText(_translate("MainWindow", "CAJA DE TEXTO"))
        self.btnMostrar.setText(_translate("MainWindow", "Mostrar MENSAJE"))
    
    @pyqtSlot()
    def on_click_mostrar(self):
        textBoxValue = self.textEditar.toPlainText()
        print(textBoxValue)
    

