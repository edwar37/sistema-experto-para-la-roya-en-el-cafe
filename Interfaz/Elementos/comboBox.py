# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ComboBox.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.seleccion = QtWidgets.QComboBox(self.centralwidget)
        self.seleccion.setGeometry(QtCore.QRect(350, 200, 69, 22))
        self.seleccion.setObjectName("seleccion")
        self.seleccion.addItem("Rojo")
        self.seleccion.addItem("Azul")
        self.seleccion.addItem("Verde")
        self.seleccion.activated[str].connect(self.onActived)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(320, 70, 131, 16))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.seleccion.setItemText(0, _translate("MainWindow", "Rojo"))
        self.seleccion.setItemText(1, _translate("MainWindow", "Azul"))
        self.seleccion.setItemText(2, _translate("MainWindow", "Verde"))
        self.label.setText(_translate("MainWindow", "COMBO BOX"))
    
    def onActived(self,text):
        print(text)

