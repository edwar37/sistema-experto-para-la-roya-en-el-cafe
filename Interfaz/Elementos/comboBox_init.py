# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 22:23:21 2017

@author: edwarjavier
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from comboBox import *

app = QApplication(sys.argv)
window = QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(window)

window.show()
sys.exit(app.exec_())