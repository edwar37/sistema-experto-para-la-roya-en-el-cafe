# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana2.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(342, 242)
        self.lblMensaje = QtWidgets.QLabel(Dialog)
        self.lblMensaje.setGeometry(QtCore.QRect(80, 30, 171, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lblMensaje.setFont(font)
        self.lblMensaje.setObjectName("lblMensaje")
        self.btnok = QtWidgets.QPushButton(Dialog)
        self.btnok.setGeometry(QtCore.QRect(140, 120, 81, 31))
        self.btnok.setObjectName("btnok")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.lblMensaje.setText(_translate("Dialog", "Mensaje desplegado!!!"))
        self.btnok.setText(_translate("Dialog", "OK"))

