# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Messagebox.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QPushButton
from ventana2 import *
from ventana3 import *
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnPulsame = QtWidgets.QPushButton(self.centralwidget)
        self.btnPulsame.setGeometry(QtCore.QRect(310, 240, 121, 31))
        self.btnPulsame.setObjectName("btnPulsame")
        self.btnPulsame.clicked.connect(self.open_window)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(310, 50, 131, 20))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName("label")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.dialogo = Dialogo()

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnPulsame.setText(_translate("MainWindow", "Pulsame!"))
        self.label.setText(_translate("MainWindow", "Messagebox"))
    
    def open_window(self):
        print('enlazado')
        print('abrir una nueva ventana')
        self.dialogo.lblMensaje.setText('Mensaje cambiado desde la ventana principal')
        self.dialogo.exec_()
        
    

