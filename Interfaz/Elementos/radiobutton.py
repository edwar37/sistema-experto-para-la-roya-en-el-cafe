# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'RadioButton.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(340, 60, 131, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.rbtn_op1 = QtWidgets.QRadioButton(self.centralwidget)
        self.rbtn_op1.setGeometry(QtCore.QRect(130, 170, 82, 17))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.rbtn_op1.setFont(font)
        self.rbtn_op1.setObjectName("rbtn_op1")
        self.rbtn_op1.zone = 1
        self.rbtn_op1.toggled.connect(self.on_radio_button_toggled)
        self.rbtn_op2 = QtWidgets.QRadioButton(self.centralwidget)
        self.rbtn_op2.setGeometry(QtCore.QRect(510, 170, 82, 17))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.rbtn_op2.setFont(font)
        self.rbtn_op2.setObjectName("rbtn_op2")
        self.rbtn_op2.zone = 2
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Radio Button"))
        self.rbtn_op1.setText(_translate("MainWindow", "01"))
        self.rbtn_op2.setText(_translate("MainWindow", "02"))
    
    def on_radio_button_toggled(self):
        if self.rbtn_op1.isChecked():
            print("Zona 1 seleccionada")
            print(self.rbtn_op1.zone)
        elif self.rbtn_op2.isChecked():
            print("Zona 2 seleccionada")
            print(self.rbtn_op2.zone)
        

