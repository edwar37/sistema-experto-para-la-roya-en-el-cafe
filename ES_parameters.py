# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 14:46:03 2017

@author: edwarjavier
"""
from pyknow import Fact

import ES_calculate

class Parameters(Fact):
    """Esta clase establece los parámetros de entrada al sistema experto.
    
    Parámetros de entrada: 
        zone: (1,2)
        Tmax: temperatura máxima en grados centigrados.
        Tmin: temperatura minima en grados centigrados.
        pl: pluviosidad en milímetros.
        humidity: humedad relativa en porcentaje.
        date: mes del año en el que fueron tomados los datos (1,2,3,...,12)
        altitude: metros
        
    Parámetros de salida:
        zone: (1,2) 
        delta_T: (small, large) 
        month: (dry, wet,very_wet) 
        humidity = (greater90, smaller90, greater95, smaller95) 
        date: (1,2,3,4) 
    """
        
    def processing_attributes(self, zoneIn1, dateIn1, tmin1, tmax1, pl1, h1, altitude1):
        """
        Este método realiza el procesamiento de los datos de entrada para generar las salidas
        al sistema experto.
        
        Métodos utilizados:
            Calculation()
            start() -- > dispara las reglas del sistema experto.
        """
        h = float(h1)
        zoneIn = int(zoneIn1)
        dateIn = int(dateIn1)
        tmin = float(tmin1)
        tmax = float(tmax1)
        pl = float(pl1)
        altitude = int(altitude1)
        days = 28
        print('Parametros: ')
        print(h, zoneIn, dateIn, tmin, tmax, pl, altitude)
    
        obj = ES_calculate.Calculation()
        humidity = obj.humidity(h)
        zone = obj.zone(zoneIn)
        delta_T = obj.amplitude_T(tmin,tmax)
        month = obj.month(pl,altitude,days)
        date = obj.trimester(dateIn)
        import ES_run_init
        ES_run_init.ES_run_init().start(humidity, zone, delta_T, month, date)
        
    
    
    
    
    
