# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 14:49:15 2017

@author: edwarjavier
'Este archivo ejecuta al sistema experto.'
"""
from ES_rules import Rules
from ES_parameters import Parameters

class SE:
    
    'Esta clase declara las entradas del SE.'
    
    def se1(self, humidity1, zone1, delta_T1, month1, date1):
        """
        Este método asigna las variables de entrada al sistema experto y ejecuta las reglas.
        """
        engine = Rules()
        engine.reset()
        engine.declare(Parameters(zone = zone1, 
                                  delta_T = delta_T1,
                                  month = month1,
                                  date = date1,
                                  humidity = humidity1))
        engine.run()
        

