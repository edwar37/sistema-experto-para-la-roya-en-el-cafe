# -*- coding: utf-8 -*-
#########################Módulo principal##############################
"""
Created on Thu Nov 23 10:06:02 2017

@author: edwarjavier"""

from pyknow import *
from ES_parameters import Parameters
from ES_answers import Answers
class Rules(KnowledgeEngine):
    """Esta clase define las reglas del sistema experto.
        
        Parámetros de entrada: 
            zone: (1,2)
            delta_T: (small, large)
            month: (dry, wet,very_wet)
            humidity = (greater90, smaller90)
            date: (1,2,3,4)
        Respuesta de todas las reglas:
            Nivel 'alto', 'medio' o 'bajo' de roya.
    """
    
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule01(self):
        'Este método ejecuta la regla 1: Nivel bajo de roya con alerta de posibilidad de inicio de la enfermedad'
        
        Answers().slow_level()
        print("Nivel bajo")
        print("Es posible que el hongo se encuentre en sus primeras fases de desarrollo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule02(self):
        'Este método ejecuta la regla 2:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule03(self):
        'Este método ejecuta la regla 3:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'very_wet')))
    def rule04(self):
        'Este método ejecuta la regla 4:Nivel bajo de roya. La lluvia está limpiando las hojas'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule05(self):
        'Este método ejecuta la regla 5:Nivel bajo de roya.'
        
        print("Nivel bajo")

    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule06(self):
        'Este método ejecuta la regla 6:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule07(self):
        'Este método ejecuta la regla 7:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule08(self):
        'Este método ejecuta la regla 8:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule09(self):
        'Este método ejecuta la regla 9:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule10(self):
        'Este método ejecuta la regla 10:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule11(self):
        'Este método ejecuta la regla 11:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule12(self):
        'Este método ejecuta la regla 12:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule13(self):
        'Este método ejecuta la regla 13:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule14(self):
        'Este método ejecuta la regla 14:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule15(self):
        'Este método ejecuta la regla 15:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule16(self):
        'Este método ejecuta la regla 16:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule17(self):
        'Este método ejecuta la regla 17:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule18(self):
        'Este método ejecuta la regla 18:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule19(self):
        'Este método ejecuta la regla 19:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule20(self):
        'Este método ejecuta la regla 20:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule21(self):
        'Este método ejecuta la regla 21:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule22(self):
        'Este método ejecuta la regla 22:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'very_wet')))
    def rule23(self):
        'Este método ejecuta la regla 23:Nivel bajo de roya. La lluvia esta limpiando las hojas'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule24(self):
        'Este método ejecuta la regla 24:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule25(self):
        'Este método ejecuta la regla 25:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule26(self):
        'Este método ejecuta la regla 26:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule27(self):
        'Este método ejecuta la regla 27:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule28(self):
        'Este método ejecuta la regla 28:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule29(self):
        'Este método ejecuta la regla 29:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule30(self):
        'Este método ejecuta la regla 30:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule31(self):
        'Este método ejecuta la regla 31:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule32(self):
        'Este método ejecuta la regla 32:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'small'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule33(self):
        'Este método ejecuta la regla 33:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule34(self):
        'Este método ejecuta la regla 34:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule35(self):
        'Este método ejecuta la regla 35:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule36(self):
        'Este método ejecuta la regla 36:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule37(self):
        'Este método ejecuta la regla 37:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 4), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule38(self):
        'Este método ejecuta la regla 38:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule39(self):
        'Este método ejecuta la regla 39:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule40(self):
        'Este método ejecuta la regla 40:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule41(self):
        'Este método ejecuta la regla 41:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'small'), Parameters(month = 'very_wet')))
    def rule42(self):
        'Este método ejecuta la regla 42:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule43(self):
        'Este método ejecuta la regla 43:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule44(self):
        'Este método ejecuta la regla 44:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule45(self):
        'Este método ejecuta la regla 45:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 2), Parameters(delta_T = 'large'), Parameters(month = 'very_wet')))
    def rule46(self):
        'Este método ejecuta la regla 46:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule47(self):
        'Este método ejecuta la regla 47:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'wet')))
    def rule48(self):
        'Este método ejecuta la regla 48:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'very_wet')))
    def rule49(self):
        'Este método ejecuta la regla 49:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule50(self):
        'Este método ejecuta la regla 50:Nivel bajo de roya. Alerta de posibilidad de inicio de la enfermedad'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'wet')))
    def rule51(self):
        'Este método ejecuta la regla 51:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 1), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'very_wet')))
    def rule52(self):
        'Este método ejecuta la regla 52:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule53(self):
        'Este método ejecuta la regla 53:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule54(self):
        'Este método ejecuta la regla 54:Nivel medio de roya.'        
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule55(self):
        'Este método ejecuta la regla 55:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'small'), Parameters(month = 'very_wet')))
    def rule56(self):
        'Este método ejecuta la regla 56:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule57(self):
        'Este método ejecuta la regla 57:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule58(self):
        'Este método ejecuta la regla 58:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule59(self):
        'Este método ejecuta la regla 59:Nivel bajo de roya.'
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 1), Parameters(delta_T = 'large'), Parameters(month = 'very_wet')))
    def rule60(self):
        'Este método ejecuta la regla 60:Nivel bajo de roya.'        
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'dry')))
    def rule61(self):
        'Este método ejecuta la regla 61:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule62(self):
        'Este método ejecuta la regla 62:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule63(self):
        'Este método ejecuta la regla 63:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule64(self):
        'Este método ejecuta la regla 64:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'small'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule65(self):
        'Este método ejecuta la regla 65:Nivel bajo de roya.'        
        
        print("Nivel bajo")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'dry')))
    def rule66(self):
        'Este método ejecuta la regla 66:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'greater90')))
    def rule67(self):
        'Este método ejecuta la regla 67:Nivel alto de roya.'
        
        print("Nivel alto")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'wet'), Parameters(humidity = 'smaller90')))
    def rule68(self):
        'Este método ejecuta la regla 68:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'greater90')))
    def rule69(self):
        'Este método ejecuta la regla 69:Nivel medio de roya.'
        
        print("Nivel medio")
    
    @Rule(AND(Parameters(zone = 2), Parameters(date = 3), Parameters(delta_T = 'large'), Parameters(month = 'very_wet'), Parameters(humidity = 'smaller90')))
    def rule70(self):
        'Este método ejecuta la regla 70:Nivel alto de roya.'        
        
        print("Nivel alto")