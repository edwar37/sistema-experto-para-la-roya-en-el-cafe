# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 14:03:59 2017

@author: edwarjavier

Métodos de pruebas unitarias
"""
import sys
sys.path.append("..")

from ES_rules import Rules
from ES_parameters import Parameters

engine = Rules()
engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'dry',
                          date = 4))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 4,
                          humidity = 'greater90'
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 4,
                          humidity = 'smaller90'
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'very_wet',
                          date = 4                         
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'dry',
                          date = 4                         
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 4,
                          humidity = 'greater90'
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 4,
                          humidity = 'smaller90'
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 4,
                          humidity = 'greater95'
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 4,
                          humidity = 'smaller95'
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'dry',
                          date = 1                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 1,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 1,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'very_wet',
                          date = 1,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'small',
                          month = 'very_wet',
                          date = 1,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'dry',
                          date = 1                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 1,
                          humidity = 'greater90'                          
                          ))
engine.run()
    
engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 1,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 1,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 1, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 1,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'dry',
                          date = 2                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 2,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 2,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'very_wet',
                          date = 2                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'dry',
                          date = 2                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 2,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 2,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 2,
                          humidity = 'greater95'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 2,
                          humidity = 'smaller95'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'dry',
                          date = 4                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 4,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'wet',
                          date = 4,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'very_wet',
                          date = 4,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'small',
                          month = 'very_wet',
                          date = 4,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'dry',
                          date = 4                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 4,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'wet',
                          date = 4,
                          humidity = 'smaller90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 4,
                          humidity = 'greater90'                          
                          ))
engine.run()

engine.reset()
engine.declare(Parameters(zone = 2, 
                          delta_T = 'large',
                          month = 'very_wet',
                          date = 4,
                          humidity = 'smaller90'                          
                          ))
engine.run()
