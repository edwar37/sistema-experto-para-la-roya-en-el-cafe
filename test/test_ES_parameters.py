# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 11:39:46 2017

@author: edwarjavier
"""

import sys
sys.path.append("..")

import ES_calculate

obj = ES_calculate.Calculation()

def test_zone():
    assert obj.zone(1) == 1

def test_trimester():
    assert obj.trimester(5) == 2

def test_amplitude():
    assert obj.amplitude(3,4) == 1
    assert obj.amplitude(2,7) == 5

def test_amplitude_T():
    assert obj.amplitude_T(3,4) == 'small'
    assert obj.amplitude_T(3,80) == 'large'

def test_eto():
    assert str(obj.eto(1134))[0:4] == '3.48'

def test_month():
    assert obj.month(34, 1134, 28) == 'dry'
    assert obj.month(100, 1134, 28) == 'wet'
    assert obj.month(224, 1134, 28) == 'very_wet'

def test_calculate_mean():
    assert obj.calculate_mean([1]) == 1
    assert obj.calculate_mean([1,2]) == 1.5
    assert obj.calculate_mean([1,2,3,4,5,6,7,8,9,10]) == 5.5

def test_humidity():
    assert obj.humidity(89) == 'smaller90'
    assert obj.humidity(90) == 'smaller90'
    assert obj.humidity(91) == 'greater90'
     
    
    