# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 12:29:10 2017

@author: edwarjavier
"""



class ES_run_init:
    'Esta clase inicia el motor de inferencia del sistema experto.'
    def start(self, humidity, zone, delta_T, month, date):
        """
        Este método importa el módulo de inferencia del SE.
        
        Métodos utilizados:
            se1()
        """
        import ES_run
        ES_run.SE().se1(humidity, zone, delta_T, month, date)
    