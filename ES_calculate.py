# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 11:52:11 2017

@author: edwarjavier
"""

import numpy as np
from math import e
class Calculation():
    
    def zone(self, zone):
        'Retorna el valor de la zona: (1,2)'
        
        return zone
    
    def trimester(self, date):
        """Retorna el trimestre en el que el registro fue tomado: (1,2,3,4).
        
        Parámetros de entrada:
            Mes: 1 (enero),2,3,4...,11,12 (diciembre)
            
        Salida:
            Trimestre: (1,2,3,4)
        """
        if(date==1 or date==2 or date==3):
            trimester = 1
        elif(date==4 or date==5 or date==6):
            trimester = 2
        elif(date==7 or date==8 or date==9):
            trimester = 3
        else:
            trimester = 4
            
        return trimester
    
    def amplitude(self, min_value, max_value):
        """
        Calcula la amplitud de una variable.
        
        Parámetros de entrada: 
            Valor máximo.
            Valor minimo.
        
        Operación:
            Amplitud = Valor máximo - valor mínimo
        
        Salida:
            Amplitud: número
                
        """
        delta_value = max_value - min_value
                
        
        
        return delta_value
    
    def amplitude_T(self, TMIN, TMAX):
        """
        Define si la amplitud de temperatura es mayor o menor que 12.
        
        Parámetros de entrada:
            TMAX.
            TMIN.
        
        Métodos externos usados:
            amplitude(min_value, max_value)
        
        Salida:
            small: valor menor o igual a 12.
            large: valor mayor que 12.
        """
        amplitude = Calculation().amplitude(TMIN, TMAX)
        
        if(amplitude<=12):
            delta_T = 'small'
        else:
            delta_T = 'large'
        
        return delta_T
    
    def eto(self, altitude):
        """Calcula el valor de la evapotranspiración
        de referencia media (eto).
        
        Parámetros de entrada:
            altitude: en metros.
        
        Operación:
            eto = 4,37e**(-0,0002*altitud)
        
        Salida:
            eto diaria
        """
        result = 4.37*pow(e, (-0.0002*altitude))
        
        return result
    
    def month(self, pl, altitude, days):
        """
        Este método define en un rango de días si el periodo de tiempo ha sido seco, húmedo o muy húmedo.
        
        Parámetros de entrada:
            pl: pluviosidad total en milímetros durante los días analizados.
            altitude: altitud del lugar de origen de los datos.
            days: número de días que suman el total de la pluviosidad analizada.
        
        Métodos externos usados:
            eto(altitude)
        
        Salida:
            month: (dry, wet, very_wet)
        """
        eto = Calculation().eto(altitude)
        eto_total = eto*days
        
        if(pl<eto_total):
            month = 'dry'
        elif(pl>=eto_total and pl<=2*eto_total):
            month = 'wet'
        else:
            month = 'very_wet'
        
        return month
    
    def calculate_mean(self, list_values):
        ' Returna el valor medio de los valores de la lista de entrada.'
        
        return np.mean(list_values)
    
    def humidity(self, humidity_mean):
        """
        Retorna una característica de la humedad dependiendo de su valor de entrada.
        
        Parámetros de entrada: 
            Humidity: valor de humedad relativa en porcentaje.
        
        Salida:
            smaller90: valor menor o igual a 90%.
            greater90: valor mayor que 90%.
        """
        if(humidity_mean<=90):
            result = 'smaller90'
        else:
            result = 'greater90'
        
        return result
        